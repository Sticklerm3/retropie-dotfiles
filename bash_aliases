# youtube-dl
alias ytdl=youtube-dl
alias ytd=youtube-dl
alias ytx="youtube-dl -x --audio-format mp3"


# some more ls aliases
alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'

alias bye=exit
alias q=bye
alias herstory=history
alias h=herstory

alias tcmount="sudo mount.cifs //10.0.0.85/Kamino /home/pi/starkiller-base/ -o user=pi,password=raspberry,file_mode=0777,dir_mode=0777,sec=ntlm,vers=1.0"
