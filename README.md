# Retropie Dotfiles

These files are managed using [thoughtbot/rcm](https://github.com/thoughtbot/rcm). 

## Setup

Insert your sd card into your laptop and open a terminal.  Go to the boot drive and enable ssh:

```sh
$ cd /Volumes/boot
$ touch ssh
```

To preconfigure WiFi: 

```sh
nano wpa_supplicant.conf
```
and paste the following replacing `YOUR_SSID` with your network SSID and `YOUR_WIFI_PASSWORD` with your network password. The first time the system is booted it will connect toy your WiFi automatically. 

```sh
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
network={
    ssid="YOUR_SSID"
    psk="YOUR_WIFI_PASSWORD"
    key_mgmt=WPA-PSK
}
```

## `rcm` on raspbian

To install `rcm`: 

```sh
wget -qO - https://apt.thoughtbot.com/thoughtbot.gpg.key | sudo apt-key add -
echo "deb https://apt.thoughtbot.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/thoughtbot.list
sudo apt-get update
sudo apt-get install rcm
```

## Oh My Zsh

[Homepage](https://ohmyz.sh)

Install: 

```bash
sh -c “$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)”
``` 

### Plugins

#### Powerlevel10k

```zsh
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git $ZSH_CUSTOM/themes/powerlevel10k
```

Set `ZSH_THEME=powerlevel10k/powerlevel10k` in your `~/.zshrc`.

## Google AIY

My current setup uses a raspberry pi zero w with the google sound HAT. The github page can be found [here](https://github.com/google/aiyprojects-raspbian).  

Install AIY software on an existing Raspbian system

Follow these steps to install the AIY drivers and software onto an existing Raspbian system.

> **Note:** This process is compatible with Raspbian Buster (2019-06-20) or later only.
Before you start, be sure you have the latest version of [Raspbian][raspbian].

1. Add the AIY Debian packages repo

Add AIY package repo:

```bash
echo "deb https://packages.cloud.google.com/apt aiyprojects-stable main" | sudo tee /etc/apt/sources.list.d/aiyprojects.list
```

Add Google package keys:

```bash
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
```

Update and install the latest system updates (including kernel):

```bash
sudo apt-get update
sudo apt-get upgrade
```

Reboot after update:

```bash
sudo reboot
```

2. Install optional packages

RGB Button Driver

This package is needed only if you're using the light-up RGB button that's included with
the Vision/Voice Bonnet:

```bash
sudo apt-get install -y leds-ktd202x-dkms
```

Run `sudo modprobe leds_ktd202x` to load the driver and `sudo modprobe -r leds_ktd202x` to
unload it. Vision/Voice Bonnet does this automatically via built-in device tree overlay
saved in the board's EEPROM.

Support for AIY Projects app

In order to make the Pi work with the [AIY Projects][aiy-app] app:

```bash
sudo apt-get install -y aiy-bt-prov-server
```

 3. Install required packages

Use the following commands to install packages for either the
[Vision Bonnet](#install-vision-bonnet-packages) or the
[Voice Bonnet/HAT](#install-voice-bonnethat-packages).


 Install Voice Bonnet/HAT packages

Install the bonnet/HAT drivers:

```bash
sudo apt-get install -y aiy-voicebonnet-soundcard-dkms
```

Disable built-in audio:

```bash
sudo sed -i -e "s/^dtparam=audio=on/#\0/" /boot/config.txt
```

Install PulseAudio:

```bash
sudo apt-get install -y pulseaudio
sudo mkdir -p /etc/pulse/daemon.conf.d/
echo "default-sample-rate = 48000" | sudo tee /etc/pulse/daemon.conf.d/aiy.conf
```

If you want to use Google Assistant, install the Raspberry-Pi-compatible
`google-assistant-library` python library:

```bash
sudo apt-get install -y aiy-python-wheels
```

Reboot:

```bash
sudo reboot
```

Then verify that you can record audio:

```bash
arecord -f cd test.wav
```

...and play a sound:

```bash
aplay test.wav
```

Additionally, the Voice Bonnet/HAT requires access to Google Cloud APIs.
To complete this setup, follow the [Voice Kit setup instructions][aiy-voice-setup].

 4. Install the AIY Projects Python library

Finally, you need to install the [AIY Projects Python library](
https://aiyprojects.readthedocs.io/en/latest/index.html).

First make sure you have `git` installed:

```bash
sudo apt-get install -y git
```

Then clone this `aiyprojects-raspbian` repo from GitHub:

```bash
git clone https://github.com/google/aiyprojects-raspbian.git AIY-projects-python
```

And now install the Python library in editable mode:

```bash
sudo pip3 install -e AIY-projects-python
```

## iTerm 2 integration 

Both my `bashrc` and `zshrc` have lines for integration with [iTerm2](https://www.iterm2.com).  To install bash integration: 


```bash
curl -L https://iterm2.com/shell_integration/bash \
-o ~/.iterm2_shell_integration.bash
```

To install `zsh` integration: 


```bash
curl -L https://iterm2.com/shell_integration/zsh \
-o ~/.iterm2_shell_integration.zsh
```
## New Install to-do's

config wifi
Install oh-my-zsh
create ssh key
config ssh
