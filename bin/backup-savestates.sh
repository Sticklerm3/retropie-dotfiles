#!/bin/bash

rsync -av --exclude '/backups' --include '*/' --include '*.srm*' --include '*.bsv*' --include '*.sav*' --include '*.sta*' --include '*.fs*' --include '*.nv*' --include '*.rtc*' --exclude '*' /home/pi/RetroPie/roms/ /home/pi/RetroPie/roms/backups

sleep 3

